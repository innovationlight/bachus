from django import forms
from django.forms import ModelForm
from .models import Fastq, Pipeline
from main.models import Project, Panel
from django.db.models import Q

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit


class PipelineForm(ModelForm):
    project = forms.ModelChoiceField(
        label='Project',
        queryset=None)

    class Meta:
        model = Pipeline
        exclude = ['user', 'entry_date']

    def __init__(self, user, *args, **kwargs):
        super(PipelineForm, self).__init__(*args, **kwargs)
        self.fields['project'].queryset = Project.objects.filter(user=user)
        if not user.is_staff:
            del self.fields['curated']

        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', 'Submit'))


class RunForm(forms.Form):
    panel = forms.ModelChoiceField(
        label='Panel',
        queryset=None)
    pipeline = forms.ModelChoiceField(
        label='Config',
        queryset=None)
    input_files = forms.CharField(label='input files', widget=forms.Textarea)
    input_names = forms.CharField(
        label='input names', widget=forms.Textarea, required=False)

    def __init__(self, user, project_pk, *args, **kwargs):
        super(RunForm, self).__init__(*args, **kwargs)
        self.fields['panel'].queryset = Panel.objects.filter(
            Q(curated=True, user__is_superuser=True) | Q(user=user))
        self.fields['pipeline'].queryset = Pipeline.objects.filter(
            Q(curated=True, user__is_superuser=True) | Q(user=user))

        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', 'Submit'))


class FastqForm(ModelForm):
    class Meta:
        model = Fastq
        exclude = ['entry_date', 'user']
        fields = ['project', 'panel', 'fastq_file']
        widgets = {
            'project': forms.Select(
            ),
            'panel': forms.Select(
            ),
            'fastq_file': forms.ClearableFileInput(
                attrs={
                    'multiple': True,
                }
            )
        }

    def __init__(self, user, *args, **kwargs):
        super(FastqForm, self).__init__(*args, **kwargs)
        self.fields['project'].queryset = Project.objects.filter(user=user)
        self.fields['panel'].queryset = Panel.objects.filter(
            Q(curated=True,
                user__is_superuser=True) | Q(user=user))

        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', 'Submit'))
