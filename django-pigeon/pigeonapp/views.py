from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404

from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required

from django.contrib import messages

from .forms import FastqForm, PipelineForm, RunForm
from .models import Fastq, Pipeline, Run
from .tables import FastqTable, PipelineTable, RunTable, PipelineCuratedTable
from .tasks import run_pipeline
from .utils import get_config_content, get_result

from main.models import Project, Panel

from django_tables2.config import RequestConfig
from django_tables2.export.export import TableExport

from collections import defaultdict


@login_required
def index(request):
    data = [
        {
            'collection_title': 'Run',
            'rows': [
                {
                    'name': 'Start',
                    'link': 'pigeon:run_start',
                },
                {
                    'name': 'Browse',
                    'link': 'pigeon:run_browse',
                }
            ]
        },
        {
            'collection_title': 'Fastq',
            'rows': [
                {
                    'name': 'Upload',
                    'link': 'pigeon:fastq_upload',
                },
                {
                    'name': 'Browse',
                    'link': 'pigeon:fastq_browse',
                }
            ]
        },
        {
            'collection_title': 'Pipeline',
            'rows': [
                {
                    'name': 'Create',
                    'link': 'pigeon:pipeline_create',
                },
                {
                    'name': 'Browse',
                    'link': 'pigeon:pipeline_browse',
                },
                {
                    'name': 'Browse Curated',
                    'link': 'pigeon:pipeline_browse_curated',
                },
            ]
        },
        # {
        #      'link': 'panel_new',
        #      'name': 'Create new panel here.',
        # },
        # {
        #      'link': 'browse_pipeline_results',
        #      'name': 'View previous runs results here.',
        # },
        # {
        #      'link': 'bed_create',
        #      'name': 'Create new bed file here.',
        # },
        # {
        #      'link': 'genes_diseases',
        #      'name': 'Search for genes and diseases.',
        # },
        # {
        #      'link': 'genes_panels',
        #      'name': 'Search for genes in panels.',
        # },
    ]

    return render(
        request=request,
        template_name='index.html',
        context={
            "data": data,
            "title": 'Pigeon',
        }
    )


@login_required
def fastq_upload(request):
    title = 'Upload Fastq'
    note = """
    <p>
    Fastq file names ideally should be in the following format:
    </br>
    <span style="color:red;display: inline-block;">samplename</span>_S(n)_L00(n)_R(n)_00(n).
    </br>
    <span style="color:red;">
    Make sure samplename part doesn\'t contain any underscores.
    </span>
    </p>
    """

    project = Project.objects.filter(
        pk=request.session.get('current_project_pk')
    ).first()
    panel = Panel.objects.filter(
        pk=request.session.get('current_panel_pk')
    ).first()
    if request.method == 'POST':
        form = FastqForm(request.user, request.POST, request.FILES)
        if form.is_valid():
            fastq_form = form.save(commit=False)
            fastq_form.project = form.cleaned_data.get('project')
            fastq_form.panel = form.cleaned_data.get('panel')
            fastq_files = request.FILES.getlist('fastq_file')

            fastq_form.user = request.user

            for fastq_file in fastq_files:
                fastq_form.pk = None
                fastq_form.fastq_file = fastq_file
                fastq_form.save()
                messages.success(
                    request, f"Fastq uploaded: {fastq_form.fastq_file.name}")

            return redirect(
                'pigeon:index'
            )
        else:
            for msg in form.errors:
                messages.error(request, f"{msg}: {form.errors[msg]}")

            return render(
                request=request,
                template_name='form.html',
                context={
                    'title': title,
                    'form': form,
                    'note': note,
                }
            )

    else:
        form = FastqForm(
            request.user,
            initial={
                'project': project,
                'panel': panel,
            },
        )
    return render(
        request=request,
        template_name='form.html',
        context={
            'title': title,
            'form': form,
            'note': note,
        }
    )


@login_required
def fastq_browse(request):
    table = FastqTable(
        Fastq.objects.filter(
            project__pk=request.session.get('current_project_pk')
        ).filter(user=request.user)
    )

    RequestConfig(request).configure(table)

    export_format = request.GET.get('_export', None)
    if TableExport.is_valid_format(export_format):
        exporter = TableExport(export_format, table)
        return exporter.response('table.{}'.format(export_format))

    return render(
        request=request,
        template_name='browse.html',
        context={
            'tables': [
                {
                    'title': 'Projects',
                    'table': table
                },
            ]
        }
    )


@login_required
def fastq_update(request, pk):
    fastq = get_object_or_404(Fastq, pk=pk)
    form = FastqForm(request.user, request.POST or None, instance=fastq)
    if form.is_valid():
        form.save()
        messages.success(
            request, f"Fastq updated: {fastq}")
        return redirect('pigeon:fastq_browse')
    return render(
        request=request,
        template_name='form.html',
        context={
            'form': form
        }
    )


@staff_member_required
def pipeline_create(request):
    project = Project.objects.filter(
        pk=request.session.get('current_project_pk')
    ).first()

    if request.method == 'POST':
        form = PipelineForm(request.user, request.POST, request.FILES)
        if form.is_valid():
            pipeline_form = form.save(commit=False)
            pipeline_form.user = request.user
            pipeline_form.save()

            messages.success(
                request, f"New pipeline created: {pipeline_form.name}")

            return redirect(
                'pigeon:index'
            )
        else:
            for msg in form.errors:
                messages.error(request, f"{msg}: {form.errors[msg]}")

            return render(
                request=request,
                template_name='form.html',
                context={
                    'title': 'New Pipeline',
                    'form': form,
                }
            )
    else:
        form = PipelineForm(
            request.user,
            initial={
                'project': project,
            },
        )
    return render(
        request=request,
        template_name='form.html',
        context={
            'title': 'New Pipeline',
            'form': form,
        }
    )


@login_required
def pipeline_browse(request):
    table = PipelineTable(
        Pipeline.objects.filter(
            project__pk=request.session.get('current_project_pk')
        ).filter(user=request.user)
    )

    RequestConfig(request).configure(table)

    export_format = request.GET.get('_export', None)
    if TableExport.is_valid_format(export_format):
        exporter = TableExport(export_format, table)
        return exporter.response('table.{}'.format(export_format))

    return render(
        request=request,
        template_name='browse.html',
        context={
            'tables': [
                {
                    'title': 'Pipelines',
                    'table': table
                },
            ]
        }
    )


@login_required
def pipeline_browse_curated(request):
    table = PipelineCuratedTable(
        Pipeline.objects.filter(curated=True, user__is_superuser=True)
    )

    RequestConfig(request).configure(table)

    export_format = request.GET.get('_export', None)
    if TableExport.is_valid_format(export_format):
        exporter = TableExport(export_format, table)
        return exporter.response('table.{}'.format(export_format))

    return render(
        request=request,
        template_name='browse.html',
        context={
            'tables': [
                {
                    'title': 'Projects',
                    'table': table
                },
            ]
        }
    )


@login_required
def run_browse(request):
    table = RunTable(
        Run.objects.filter(
            project__pk=request.session.get('current_project_pk')
        ).filter(user=request.user)
    )

    RequestConfig(request).configure(table)
    export_format = request.GET.get('_export', None)
    if TableExport.is_valid_format(export_format):
        exporter = TableExport(export_format, table)
        return exporter.response('table.{}'.format(export_format))

    return render(
        request=request,
        template_name='browse.html',
        context={
            'tables': [
                {
                    'title': 'Projects',
                    'table': table
                },
            ]
        }
    )


@login_required
def pipeline_update(request, pk):
    pipeline = get_object_or_404(Pipeline, pk=pk)
    form = PipelineForm(request.user, request.POST or None, instance=pipeline)
    if form.is_valid():
        form.save()
        messages.success(
            request, f"Pipeline updated: {pipeline}")
        return redirect('pigeon:pipeline_browse')
    return render(
        request=request,
        template_name='form.html',
        context={
            'form': form
        }
    )


@login_required
def fastq_delete(request, pk):
    fastq = get_object_or_404(Fastq, pk=pk)
    fastq.delete()
    messages.success(
        request, f"Deleted fastq {fastq.fastq_file.name}")
    return redirect('pigeon:fastq_browse')


@login_required
def pipeline_delete(request, pk):
    pipeline = get_object_or_404(Pipeline, pk=pk)
    pipeline.delete()
    messages.success(
        request, f"Deleted pipeline {pipeline.name}")
    return redirect('pigeon:pipeline_browse')


@login_required
def run_delete(request, pk):
    run = get_object_or_404(Run, pk=pk)
    run.delete()
    messages.success(
        request, f"Deleted run {run}")
    return redirect('pigeon:run_browse')


@login_required
def pipeline_current(request, pk):
    pipeline = get_object_or_404(Pipeline, pk=pk)
    request.session['current_pipeline_pk'] = pk
    request.session['current_pipeline_name'] = pipeline.name
    messages.success(
        request, f"Current  pipeline set to {pipeline.name}")
    return redirect('pigeon:index')


@login_required
def run_start(request):
    project_pk = request.session.get('current_project_pk')
    project = Project.objects.filter(
        pk=request.session.get('current_project_pk')
    ).first()
    panel = Panel.objects.filter(
        pk=request.session.get('current_panel_pk')
    ).first()
    pipeline = Pipeline.objects.filter(
        pk=request.session.get('current_pipeline_pk')
    ).first()
    fastqs = Fastq.objects.filter(
        project__pk=request.session.get('current_project_pk')
    ).filter(user=request.user)

    grouped = defaultdict(list)
    for fastq in fastqs:
        filename = str(fastq)
        parts = filename.split('_')[:1]
        key = '_'.join(parts)
        grouped[key].append(filename)
    input_files = ' '.join(
        sum(grouped.values(), [])
    )
    input_names = ' '.join(
        list(grouped.keys())
    )

    if request.method == 'POST':
        form = RunForm(
            request.user,
            project_pk,
            request.POST
        )
        if form.is_valid():
            panel = form.cleaned_data.get('panel')
            pipeline = form.cleaned_data.get('pipeline')

            input_files = form.cleaned_data.get('input_files')
            input_names = form.cleaned_data.get('input_names')

            run, config_content = get_config_content(
                request.user,
                pipeline,
                project,
                panel,
                input_files,
                input_names
            )

            run_pipeline.delay(
                request.user.id,
                run.id,
                config_content,
            )

            messages.success(
                request, f"Run started: {run}")

            return redirect(
                'pigeon:index'
            )
        else:
            for msg in form.errors:
                messages.error(request, f"{msg}: {form.errors[msg]}")

            return render(
                request=request,
                template_name='form.html',
                context={
                    'title': 'Start Run',
                    'form': form,
                }
            )
    else:
        form = RunForm(
            request.user,
            project_pk,
            initial={
                'project': project,
                'panel': panel,
                'pipeline': pipeline,
                'input_files': input_files,
                'input_names': input_names,
            }
        )
    return render(
        request=request,
        template_name='form.html',
        context={
            'title': 'Start Run',
            'form': form
        }
    )


@login_required
def run_result(request, pk):
    run = get_object_or_404(Run, pk=pk)

    data = get_result(run)

    return render(
        request=request,
        template_name='index.html',
        context={
            'data': data,
        }
    )
