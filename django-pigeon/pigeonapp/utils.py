from django.conf import settings
import os

from .models import Fastq, Run
from doveapp.models import Vcf

from doveapp.utils import create_vcf_model, create_annotation_model

import configparser
import datetime

from io import StringIO


def get_config_content(user, pipeline, project, panel, input_files=None, input_names=None):
    datetime_stamp = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    run_folder = os.path.join(
        'user_data',
        '{}'.format(user.id),
        project.name,
        'runs',
        '{}_{}_{}'.format(
            panel.name,
            pipeline.name,
            datetime_stamp,
        )
    )
    output_dir = os.path.join(settings.MEDIA_ROOT, run_folder)

    run = Run()
    run.user = user
    run.project = project
    run.panel = panel
    run.pipeline = pipeline
    run.folder = run_folder
    run.save()

    config_content = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
    config_content.read_string(
        pipeline.config_file.read().decode('utf-8'))

    config_content['PROJECT']['project_name'] = project.name
    config_content['PROJECT']['output_dir'] = output_dir
    config_content['PROJECT']['bed_file'] = panel.bed_file.path

    if 'GENERAL' in config_content:
        for resource in config_content['GENERAL']:
            config_content['GENERAL'][resource] = os.path.join(os.path.join(
                settings.MEDIA_ROOT, 'resources'), config_content['GENERAL'][resource])

    if 'INCLUDE' in config_content:
        for include in config_content['INCLUDE']:
            config_content['INCLUDE'][include] = os.path.join(os.path.join(
                settings.MEDIA_ROOT, 'pipelines'), config_content['INCLUDE'][include])

    if input_files is not None:
        input_files = input_files.replace('\r\n', ' ')
        if input_names is not None:
            config_content['PROJECT']['input_names'] = input_names.replace(
                '\r\n', ' ')
    else:
        input_files = config_content['PROJECT']['input_files']

    input_file_paths = []
    for input_file in input_files.split(' '):
        if input_file.endswith(('.vcf', 'vcf.gz')):
            for vcf in Vcf.objects.filter(user=user):
                if input_file == str(vcf):
                    input_file_paths.append(os.path.join(
                        settings.MEDIA_ROOT, vcf.vcf_file.name))
        if input_file.endswith(('.fastq', 'fastq.gz')):
            for fastq in Fastq.objects.filter(user=user):
                if input_file == str(fastq):
                    input_file_paths.append(os.path.join(
                        settings.MEDIA_ROOT, fastq.fastq_file.name))
    config_content['PROJECT']['input_files'] = ' '.join(input_file_paths)

    str_conf = StringIO()
    config_content.write(str_conf)

    return run, str_conf.getvalue()


def get_result(run):
    wanted_files = {
        'quality_control': ['.html'],
        'alignment': ['.bam', '.bai'],
        'variant_call': ['.vcf.gz', '.vcf', '.vcf.gz.tbi'],
        'annotation': ['_annotation.csv', '_canonical.csv'],
        'run_log': ['.log'],
        'coverage': ['_summary']}
    wanted_file_extentions = tuple(
        [extention for extention_list in wanted_files.values() for extention in extention_list])

    # results = defaultdict(list)
    results = []
    for wanted_file_extention in wanted_file_extentions:
        temp_dict = {
            'collection_title': wanted_file_extention[1:],
            'rows': []
        }
        for root, dirs, file_names in os.walk(os.path.join(settings.MEDIA_ROOT, run.folder)):
            for file_name in file_names:
                if file_name.endswith(wanted_file_extention):
                    # Create file path
                    file_path = os.path.relpath(
                        os.path.join(root, file_name),
                        os.path.dirname(settings.MEDIA_ROOT)
                    )
                    # Add for results view
                    if file_name.endswith(tuple(wanted_files['variant_call'][:1] + wanted_files['alignment'][:1])):
                        temp_dict['rows'].append(
                            {
                                'name': file_name.split('_')[0],
                                'media': file_path,
                                'viewer': 'main:igv',
                                'icon': 'main/igv/img/favicon.ico'
                            }
                        )
                    else:
                        temp_dict['rows'].append(
                            {
                                'name': file_name.split('_')[0],
                                'media': '/' + file_path,
                            }
                        )
                    # Create a model instance if new
                    file_path = file_path.split('/', 1)[1]
                    if file_name.endswith(tuple(wanted_files['variant_call'][:2])):
                        create_vcf_model(run, file_path)
                    if file_name.endswith(tuple(wanted_files['annotation'])):
                        create_annotation_model(run, file_path)
        if temp_dict['rows']:
            results.append(temp_dict)
    print(results)
    return results
