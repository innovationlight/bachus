from __future__ import absolute_import, unicode_literals
from celery import shared_task

from django.core.mail import send_mail

from pigeon.core.pipeline import Pipe
from pigeon.utils.runpipe import RunPipe

from .utils import get_result

from django.contrib.auth.models import User
from .models import Run

import time
import os


@shared_task
def run_pipeline(user_id, run_id, config_content):
    user = User.objects.get(id=user_id)
    run = Run.objects.get(id=run_id)
    pipeline = Pipe(config_content, dryrun=False,
                    verbose=False, read_from='string')

    if not os.path.exists(pipeline.project_output_dir):
        os.makedirs(pipeline.project_output_dir)

    for task in pipeline.task_list:
        for cmd in pipeline.cmd_feed[task]:
            if 'pass' not in pipeline.task_parameters[task].keys():
                result = run_task.delay(
                    cmd,
                    task,
                    pipeline.project_output_dir,
                    pipeline.project_name,
                    pipeline.verbose,
                    user_id=user_id,
                    project_id=run.project.id
                )
                while not result.ready():
                    time.sleep(5)
    get_result(run)

    send_mail(
        'Run Complete',
        '{} completed'.format(str(run)),
        'genera.bioinf@gmail.com',
        ['{}'.format(user.email)],
        fail_silently=False,
    )


@shared_task
def run_task(cmd, task, output_dir, project_name, verbose, user_id, project_id):
    task_instance = RunPipe(
        cmd,
        task,
        output_dir,
        project_name,
        verbose
    )
    task_instance.run_task()
