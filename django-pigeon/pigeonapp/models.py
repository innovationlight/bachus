from django.db import models
from django.contrib.auth.models import User
from django.core.validators import FileExtensionValidator

from main.models import Project, Panel
from main.models import OverwriteStorage, get_default_user, get_default_panel

import datetime
import os


def config_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'user_data/{0}/{1}/config/{2}'.format(
        instance.user.id,
        instance.project,
        filename
    )


def fastq_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'user_data/{0}/{1}/fastq/{2}/{3}'.format(
        instance.user.id,
        instance.project,
        instance.panel,
        filename
    )


class Pipeline(models.Model):
    entry_date = models.DateField(default=datetime.datetime.now)
    user = models.ForeignKey(User, on_delete=models.SET(get_default_user))
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    name = models.SlugField(max_length=255)
    description = models.TextField()
    curated = models.BooleanField(null=True)
    config_file = models.FileField(
        upload_to=config_directory_path,
        storage=OverwriteStorage(),
        validators=[FileExtensionValidator(allowed_extensions=['conf'])]
    )

    def __str__(self):
        return self.name


class Run(models.Model):
    entry_date = models.DateField(default=datetime.datetime.now)
    user = models.ForeignKey(User, on_delete=models.SET(get_default_user))
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    panel = models.ForeignKey(Panel, on_delete=models.SET(get_default_panel))
    pipeline = models.ForeignKey(Pipeline, on_delete=models.DO_NOTHING)
    folder = models.TextField()

    def __str__(self):
        return '{} {} {} {}'.format(
            self.project.name,
            self.panel.name,
            self.pipeline.name,
            self.entry_date,
        )


class Fastq(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET(get_default_user))
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    panel = models.ForeignKey(Panel, on_delete=models.SET(get_default_panel))
    entry_date = models.DateField(default=datetime.datetime.now)
    fastq_file = models.FileField(
        upload_to=fastq_directory_path,
        storage=OverwriteStorage(),
        validators=[FileExtensionValidator(allowed_extensions=['fastq', 'gz'])]
    )

    def __str__(self):
        return os.path.basename(self.fastq_file.name)
