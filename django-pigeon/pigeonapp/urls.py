from django.urls import path, re_path

from . import views

app_name = 'pigeon'

urlpatterns = [
    path('', views.index, name='index'),
    path('index/', views.index, name='index'),

    path('fastq/upload', views.fastq_upload, name='fastq_upload'),
    path('fastq/browse', views.fastq_browse, name='fastq_browse'),
    re_path(r'^fastq/(\d+)/$', views.fastq_update, name='fastq_update'),
    re_path(r'^fastq/delete/(\d+)/$', views.fastq_delete, name='fastq_delete'),

    path('pipeline/create', views.pipeline_create, name='pipeline_create'),
    path('pipeline/browse', views.pipeline_browse, name='pipeline_browse'),
    path('pipeline/browse/curated', views.pipeline_browse_curated, name='pipeline_browse_curated'),

    re_path(r'^pipeline/(\d+)/$', views.pipeline_current, name='pipeline_current'),
    re_path(r'^pipeline/update/(\d+)/$', views.pipeline_update, name='pipeline_update'),
    re_path(r'^pipeline/delete/(\d+)/$', views.pipeline_delete, name='pipeline_delete'),

    path('run/start', views.run_start, name='run_start'),
    path('run/browse', views.run_browse, name='run_browse'),
    re_path(r'^run/(\d+)/$', views.run_result, name='run_result'),
    re_path(r'^run/delete/(\d+)/$', views.run_delete, name='run_delete'),
]
