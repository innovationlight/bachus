from django.apps import AppConfig


class PigeonappConfig(AppConfig):
    name = 'pigeonapp'
