import django_tables2 as tables
from .models import Fastq, Pipeline, Run


class FastqTable(tables.Table):
    update = tables.LinkColumn('pigeon:fastq_update', args=[
        tables.A('pk')], orderable=False, empty_values=())
    delete = tables.LinkColumn('pigeon:fastq_delete', args=[
        tables.A('pk')], orderable=False, empty_values=())

    class Meta:
        model = Fastq
        exclude = ('id', 'user')

    def render_update(self):
        return '✎'

    def render_delete(self):
        return '✘'


class PipelineTable(tables.Table):
    update = tables.LinkColumn('pigeon:pipeline_update', args=[
        tables.A('pk')], orderable=False, empty_values=())
    delete = tables.LinkColumn('pigeon:pipeline_delete', args=[
        tables.A('pk')], orderable=False, empty_values=())
    current = tables.LinkColumn('pigeon:pipeline_current', args=[
        tables.A('pk')], orderable=False, empty_values=())

    class Meta:
        model = Pipeline
        exclude = ('id', 'user')
        fields = ('current', 'entry_date', 'name', 'description',
                  'config_file', 'update', 'delete')

    def render_update(self):
        return '✎'

    def render_delete(self):
        return '✘'

    def render_current(self):
        return '✔'


class PipelineCuratedTable(tables.Table):
    current = tables.LinkColumn('pigeon:pipeline_current', args=[
        tables.A('pk')], orderable=False, empty_values=())

    class Meta:
        model = Pipeline
        exclude = ('id',)
        fields = ('current', 'entry_date', 'name', 'user',
                  'description', 'config_file')

    def render_current(self):
        return '✔'


class RunTable(tables.Table):
    result = tables.LinkColumn('pigeon:run_result', args=[
        tables.A('pk')], orderable=False, empty_values=())
    delete = tables.LinkColumn('pigeon:run_delete', args=[
        tables.A('pk')], orderable=False, empty_values=())

    class Meta:
        model = Run
        exclude = ('id', 'user')
        fields = ('result', 'entry_date', 'project',
                  'panel', 'pipeline', 'delete')

    def render_result(self):
        return 'Browse'

    def render_delete(self):
        return '✘'
