from django.contrib import admin
from guardian.admin import GuardedModelAdmin

from .models import Fastq, Pipeline, Run


class FastqAdmin(GuardedModelAdmin):

    list_filter = ['user', 'project', 'panel']


class PipelineAdmin(GuardedModelAdmin):

    list_filter = ['user', 'project']


class RunAdmin(GuardedModelAdmin):

    list_filter = ['user', 'project', 'panel']


admin.site.register(Pipeline, PipelineAdmin)
admin.site.register(Run, RunAdmin)
admin.site.register(Fastq, FastqAdmin)
