from django.shortcuts import render

from django.contrib.auth.decorators import login_required

# Create Summary
# from .utils.variantclassification import VariantClassification
# import datetime


@login_required
def index(request):
    data = [
        # {
        # 'collection_title': 'Vcf',
        # 'rows': [
        #     {
        #         'name': 'Upload',
        #         'link': 'dove:vcf_upload',
        #     },
        #     {
        #         'name': 'Browse',
        #         'link': 'dove:vcf_browse',
        #     }
        # ]
        # },
    ]

    return render(
        request=request,
        template_name='index.html',
        context={
            "data": data,
            "title": 'Picus',
            "note": 'WIP'
        }
    )
