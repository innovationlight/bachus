from django.urls import path, re_path

from . import views

app_name = 'picus'

urlpatterns = [
    path('', views.index, name='index'),
    path('index/', views.index, name='index'),
]
