from django.apps import AppConfig


class PicusappConfig(AppConfig):
    name = 'picusapp'
