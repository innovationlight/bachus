# [Bacchus](http://139.59.210.254)

Django based UI for dove and pigeon

# Installation Guide
1. Clone the project
 * > git clone https://github.com/barslmn/bacchus
 * > cd bacchus
2. Add repositories 
 * > sudo add-apt-repository $(grep -vE "^\s*#" repositories | tr "\n" " ")
3. Install packages 
 * > sudo apt install $(grep -vE "^\s*#" packages | tr "\n" " ")
4. Configure postgresql [1]
5. Configure rabbitmq [3]
6. Create a venv
 * > virtualenv -p python3 venv
 * > source venv/bin/activate
7. Install requirements
 * > pip install -r requirements
8. Copy systemd services and nginx confs


# References

### Server Setup

[1] https://www.digitalocean.com/community/tutorials/how-to-set-up-django-with-postgres-nginx-and-gunicorn-on-ubuntu-18-04

### Let's Encript

[2] https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-18-04

### Rabbitmq Server Setup

[3] http://docs.celeryproject.org/en/latest/getting-started/brokers/rabbitmq.html#setting-up-rabbitmq
