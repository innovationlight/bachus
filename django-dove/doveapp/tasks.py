from __future__ import absolute_import, unicode_literals
from celery import shared_task

from django.conf import settings
from dove.core.onlinevariantannotation import OnlineVariantAnnotation as OVA
from doveapp.models import Annotation
from django.core.mail import send_mail

from .models import Vcf
from django.contrib.auth.models import User

import os


@shared_task
def annotation_task(user_id, vcf_id, query_chunks, table_cols, omim, hpo, clinvar):
    user = User.objects.get(id=user_id)
    vcf = Vcf.objects.get(id=vcf_id)

    vcf_path = os.path.join(
        settings.MEDIA_ROOT, vcf.vcf_file.name)

    output_file = 'user_{0}/{1}/annotation/{2}/{3}'.format(
        user.id,
        vcf.project,
        vcf.panel,
        '{}.annotation.csv'.format(os.path.splitext(str(vcf))[0]))

    output_path = os.path.join(settings.MEDIA_ROOT, output_file)

    annotation = Annotation()
    annotation.vcf = vcf
    annotation.user = vcf.user
    annotation.project = vcf.project
    annotation.panel = vcf.panel
    annotation.annotation_file.name = output_file
    annotation.save()

    if not os.path.exists(os.path.dirname(output_path)):
        os.makedirs(os.path.dirname(output_path))

    ova = OVA(
        vcf_file=vcf_path,
        outputfile=output_path,
        query_chunks=query_chunks,
        table_cols=table_cols,
        omim=omim,
        hpo=hpo,
        clinvar=clinvar
    )
    ova.annotate()

    send_mail(
        'Annotation Complete',
        'Annotation of {} completed'.format(str(vcf)),
        'genera.bioinformatics@gmail.com',
        ['{}'.format(user.email)],
        fail_silently=False,
    )
