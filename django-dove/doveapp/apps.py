from django.apps import AppConfig


class DoveappConfig(AppConfig):
    name = 'doveapp'
