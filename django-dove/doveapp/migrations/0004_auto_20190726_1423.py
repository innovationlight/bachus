# Generated by Django 2.2.3 on 2019-07-26 06:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('doveapp', '0003_filterset_curated'),
    ]

    operations = [
        migrations.AlterField(
            model_name='columnfilter',
            name='name',
            field=models.SlugField(max_length=255),
        ),
        migrations.AlterField(
            model_name='filterset',
            name='name',
            field=models.SlugField(max_length=255),
        ),
    ]
