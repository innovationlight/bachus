from django.urls import path, re_path

from . import views

app_name = 'dove'

urlpatterns = [
    path('', views.index, name='index'),
    path('index/', views.index, name='index'),

    path('vcf/upload', views.vcf_upload, name='vcf_upload'),
    path('ova_vep_api', views.ova_vep_api, name='ova_vep_api'),
    path('ova_vep_local', views.ova_vep_local, name='ova_vep_local'),

    re_path(r'^variant/filter/(\d+)/$', views.variant_filter, name='variant_filter'),

    path('vcf/browse', views.vcf_browse, name='vcf_browse'),
    path('annotation/browse', views.annotation_browse, name='annotation_browse'),

    re_path(r'^vcf/update/(\d+)/$', views.vcf_update, name='vcf_update'),
    re_path(r'^annotation/update/(\d+)/$', views.annotation_update, name='annotation_update'),


    re_path(r'^vcf/delete/(\d+)/$', views.vcf_delete, name='vcf_delete'),
    re_path(r'^annotation/delete/(\d+)/$', views.annotation_delete, name='annotation_delete'),
]
