import django_tables2 as tables
from .models import Vcf, Annotation

from django.utils.html import mark_safe


class VcfTable(tables.Table):
    update = tables.LinkColumn('dove:vcf_update', args=[
        tables.A('pk')], orderable=False, empty_values=())
    delete = tables.LinkColumn('dove:vcf_delete', args=[
        tables.A('pk')], orderable=False, empty_values=())

    class Meta:
        model = Vcf
        exclude = ('id', 'user')

    def render_update(self):
        return '✎'

    def render_delete(self):
        return '✘'


class AnnotationTable(tables.Table):
    vf = tables.LinkColumn('dove:variant_filter', args=[
        tables.A('pk')], orderable=False, empty_values=())
    update = tables.LinkColumn('dove:annotation_update', args=[
        tables.A('pk')], orderable=False, empty_values=())
    delete = tables.LinkColumn('dove:annotation_delete', args=[
        tables.A('pk')], orderable=False, empty_values=())

    class Meta:
        model = Annotation
        exclude = ('id', 'user')

    def render_vf(self):
        return '♈'

    def render_update(self):
        return '✎'

    def render_delete(self):
        return '✘'


class VariantTable(tables.Table):
    CHR = tables.Column(attrs={  # 'th': {'style': 'font-size:11px'},
        'td': {'style': 'font-size:11px'}, 'tf': {'style': 'font-size:11px'}})
    POS = tables.Column(attrs={  # 'th': {'style': 'font-size:11px'},
        'td': {'style': 'font-size:11px'}, 'tf': {'style': 'font-size:11px'}})
    REF = tables.Column(attrs={  # 'th': {'style': 'font-size:11px'},
        'td': {'style': 'font-size:11px'}, 'tf': {'style': 'font-size:11px'}})
    ALT = tables.Column(attrs={  # 'th': {'style': 'font-size:11px'},
        'td': {'style': 'font-size:11px'}, 'tf': {'style': 'font-size:11px'}})
    GT = tables.Column(attrs={  # 'th': {'style': 'font-size:11px'},
        'td': {'style': 'font-size:11px'}, 'tf': {'style': 'font-size:11px'}})
    AD = tables.Column(attrs={  # 'th': {'style': 'font-size:11px'},
        'td': {'style': 'font-size:11px'}, 'tf': {'style': 'font-size:11px'}})
    DP = tables.Column(attrs={  # 'th': {'style': 'font-size:11px'},
        'td': {'style': 'font-size:11px'}, 'tf': {'style': 'font-size:11px'}})
    gene_symbol = tables.Column(attrs={  # 'th': {'style': 'font-size:11px'},
        'td': {'style': 'font-size:11px'}, 'tf': {'style': 'font-size:11px'}})
    id = tables.Column(attrs={  # 'th': {'style': 'font-size:11px'},
        'td': {'style': 'font-size:11px'}, 'tf': {'style': 'font-size:11px'}})
    minor_allele_freq = tables.Column(attrs={  # 'th': {'style': 'font-size:11px'},
        'td': {'style': 'font-size:11px'}, 'tf': {'style': 'font-size:11px'}})
    gnomad = tables.Column(attrs={  # 'th': {'style': 'font-size:11px'},
        'td': {'style': 'font-size:11px'}, 'tf': {'style': 'font-size:11px'}})
    transcript_consequence_terms = tables.Column(attrs={  # 'th': {'style': 'font-size:11px'},
        'td': {'style': 'font-size:11px'}, 'tf': {'style': 'font-size:11px'}})
    # impact = tables.Column(attrs={  # 'th': {'style': 'font-size:11px'},
    #     'td': {'style': 'font-size:11px'}, 'tf': {'style': 'font-size:11px'}})
    # biotype = tables.Column(attrs={  # 'th': {'style': 'font-size:11px'},
    #     'td': {'style': 'font-size:11px'}, 'tf': {'style': 'font-size:11px'}})
    polyphen_prediction = tables.Column(attrs={  # 'th': {'style': 'font-size:11px'},
        'td': {'style': 'font-size:11px'}, 'tf': {'style': 'font-size:11px'}})
    # polyphen_score = tables.Column(attrs={  # 'th': {'style': 'font-size:11px'},
    #     'td': {'style': 'font-size:11px'}, 'tf': {'style': 'font-size:11px'}})
    sift_prediction = tables.Column(attrs={  # 'th': {'style': 'font-size:11px'},
        'td': {'style': 'font-size:11px'}, 'tf': {'style': 'font-size:11px'}})
    # sift_score = tables.Column(attrs={  # 'th': {'style': 'font-size:11px'},
    #     'td': {'style': 'font-size:11px'}, 'tf': {'style': 'font-size:11px'}})
    hgvsc = tables.Column(attrs={  # 'th': {'style': 'font-size:11px'},
        'td': {'style': 'font-size:11px'}, 'tf': {'style': 'font-size:11px'}})
    hgvsp = tables.Column(attrs={  # 'th': {'style': 'font-size:11px'},
        'td': {'style': 'font-size:11px'}, 'tf': {'style': 'font-size:11px'}})
    clin_sig = tables.Column(attrs={  # 'th': {'style': 'font-size:11px'},
        'td': {'style': 'font-size:11px'}, 'tf': {'style': 'font-size:11px'}})

    def render_gene_symbol(self, value):
        return mark_safe(f'<a href="https://www.omim.org/search/?index=entry&search={value}">{value}</a>')

    def render_id(self, value):
        return mark_safe(f'<a href="http://www.ensembl.org/Homo_sapiens/Variation/Explore?v={value}">{value}</a>')

    def render_gnomad(self, value, record):
        return mark_safe('<a href="http://gnomad.broadinstitute.org/awesome?=&query={}">{}</a>'.format(record['id'], value))

    def render_minor_allele_freq(self, value, record):
        return mark_safe('<a href="https://www.ncbi.nlm.nih.gov/snp/?term={}">{}</a>'.format(record['id'], value))

    def render_clin_sig(self, value, record):
        return mark_safe('<a href="https://www.ncbi.nlm.nih.gov/clinvar/?=clinvar&term={}">{}</a>'.format(record['id'], value))
