from django.db import models
from django.contrib.auth.models import User
from django.core.validators import FileExtensionValidator

from main.models import Project, Panel
from main.models import OverwriteStorage, get_default_user, get_default_panel
from pigeonapp.models import Run, Pipeline

import datetime
import os


def user_directory_path_vcf(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'user_data/{0}/{1}/vcf/{2}/{3}'.format(
        instance.user.id,
        instance.project,
        instance.panel,
        filename,
    )


def user_directory_path_annotation(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'user_data/{0}/{1}/annotation/{2}/{3}'.format(
        instance.user.id,
        instance.project,
        instance.panel,
        filename
    )


class Vcf(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET(get_default_user))
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    panel = models.ForeignKey(Panel, on_delete=models.SET(get_default_panel))
    run = models.ForeignKey(
        Run, on_delete=models.CASCADE, blank=True, null=True)
    pipeline = models.ForeignKey(
        Pipeline, on_delete=models.DO_NOTHING, blank=True, null=True)
    entry_date = models.DateField(default=datetime.datetime.now)
    vcf_file = models.FileField(
        upload_to=user_directory_path_vcf,
        storage=OverwriteStorage(),
        validators=[FileExtensionValidator(
            allowed_extensions=['vcf', 'vcf.gz'])]
    )

    def __str__(self):
        return os.path.basename(self.vcf_file.name)


class Annotation(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET(get_default_user))
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    panel = models.ForeignKey(Panel, on_delete=models.SET(get_default_panel))
    vcf = models.ForeignKey(Vcf, on_delete=models.CASCADE)
    entry_date = models.DateField(default=datetime.datetime.now)
    annotation_file = models.FileField(
        upload_to=user_directory_path_annotation,
        storage=OverwriteStorage(),
    )

    def __str__(self):
        return os.path.basename(self.annotation_file.name)


class ColumnFilter(models.Model):
    column_choices = (
        ('CHR', 'CHR'),
        ('POS', 'POS'),
        ('REF', 'REF'),
        ('ALT', 'ALT'),
        ('GT', 'GT'),
        ('AD', 'AD'),
        ('DP', 'DP'),
        ('gene_symbol', 'gene_symbol'),
        ('transcript_id', 'transcript_id'),
        ('id', 'id'),
        ('minor_allele_freq', 'minor_allele_freq'),
        ('gnomad', 'gnomad'),
        ('canonical', 'canonical'),
        ('transcript_consequence_terms', 'transcript_consequence_terms'),
        ('impact', 'impact'),
        ('biotype', 'biotype'),
        ('polyphen_prediction', 'polyphen_prediction'),
        ('polyphen_score', 'polyphen_score'),
        ('sift_prediction', 'sift_prediction'),
        ('sift_score', 'sift_score'),
        ('hgvsc', 'hgvsc'),
        ('hgvsp', 'hgvsp'),
        ('clin_sig', 'clin_sig'),
        ('pubmed', 'pubmed'),
    )
    option_choices = (
        ('eq', 'equals'),
        ('ne', 'not equals'),
        ('lt', 'less than'),
        ('le', 'less than or equal'),
        ('gt', 'greater than'),
        ('ge', 'gerater than or equals'),
        ('in', 'includes'),
        ('ex', 'excludes'),
    )
    user = models.ForeignKey(User, on_delete=models.SET(get_default_user))
    entry_date = models.DateField(default=datetime.datetime.now)
    column = models.CharField(
        max_length=50,
        choices=column_choices,
        blank=False
    )
    option = models.CharField(
        max_length=2,
        choices=option_choices,
        blank=False
    )
    parameters = models.TextField()

    name = models.SlugField(
        max_length=255,
    )
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name


class FilterSet(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET(get_default_user))
    entry_date = models.DateField(default=datetime.datetime.now)
    name = models.SlugField(
        max_length=255,
    )
    description = models.TextField(blank=True, null=True)
    column_filters = models.ManyToManyField(ColumnFilter, blank=False)
    curated = models.BooleanField(null=True)

    def column_filter_names(self):
        return ' '.join([column_filter.name for column_filter in self.column_filters.all()])

    def __str__(self):
        return self.name
