from django import forms
from .models import Vcf, FilterSet, ColumnFilter
from main.models import Project, Panel
from django.db.models import Q

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit


class CreateSummaryForm(forms.Form):
    pass


class VcfForm(forms.ModelForm):
    class Meta:
        model = Vcf
        exclude = ['entry_date', 'user', 'run', 'pipeline']
        fields = ['project', 'panel', 'vcf_file']
        widgets = {
            'project': forms.Select(
            ),
            'panel': forms.Select(
            ),
            'vcf_file': forms.ClearableFileInput(
                attrs={
                    'multiple': True,
                }
            )
        }

    def __init__(self, user, *args, **kwargs):
        super(VcfForm, self).__init__(*args, **kwargs)
        self.fields['project'].queryset = Project.objects.filter(user=user)
        self.fields['panel'].queryset = Panel.objects.filter(
            Q(curated=True,
                user__is_superuser=True) | Q(user=user))

        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', 'Submit'))

# class BrowseVcfForm(forms.Form):
#     defined_choices = (
#         ('filter', 'Filter Variants'),
#         ('browse', 'Browse Vcf'),
#         ('download', 'Download Vcf')
#     )
#     vcf_file = forms.ModelMultipleChoiceField(
#         label='Vcf File',
#         queryset=None)
#     action = forms.MultipleChoiceField(
#         label='Action',
#         choices=defined_choices)

#     def __init__(self, user, *args, **kwargs):
#         super(BrowseVcfForm, self).__init__(*args, **kwargs)
#         self.fields['vcf_file'].queryset = Vcf.objects.filter(user=user)


class OVAForm(forms.Form):
    choices = (
        ('CHR', 'CHR'),
        ('POS', 'POS'),
        ('REF', 'REF'),
        ('ALT', 'ALT'),
        ('GT', 'GT'),
        ('AD', 'AD'),
        ('DP', 'DP'),
        ('amino_acids', 'amino_acids'),
        ('biotype', 'biotype'),
        ('blosum62', 'blosum62'),
        ('canonical', 'canonical'),
        ('ccds', 'ccds'),
        ('cdna_end', 'cdna_end'),
        ('cdna_start', 'cdna_start'),
        ('cds_end', 'cds_end'),
        ('cds_start', 'cds_start'),
        ('clin_sig', 'clin_sig'),
        ('codons', 'codons'),
        ('transcript_consequence_terms', 'transcript_consequence_terms'),
        ('csn', 'csn'),
        ('distance', 'distance'),
        ('domains', 'domains'),
        ('exon', 'exon'),
        ('flags', 'flags'),
        ('gene_id', 'gene_id'),
        ('gene_symbol', 'gene_symbol'),
        ('gene_symbol_source', 'gene_symbol_source'),
        ('genesplicer', 'genesplicer'),
        ('minor_allele', 'minor_allele'),
        ('minor_allele_freq', 'minor_allele_freq'),
        ('aa_allele', 'aa_allele'),
        ('aa', 'aa'),
        ('amr_allele', 'amr_allele'),
        ('amr', 'amr'),
        ('afr_allele', 'afr_allele'),
        ('afr', 'afr'),
        ('allele_string', 'allele_string'),
        ('ea_allele', 'ea_allele'),
        ('ea', 'ea'),
        ('eas_allele', 'eas_allele'),
        ('eas', 'eas'),
        ('eur_allele', 'eur_allele'),
        ('eur', 'eur'),
        ('sas_allele', 'sas_allele'),
        ('sas', 'sas'),
        ('gnomad_allele', 'gnomad_allele'),
        ('gnomad', 'gnomad'),
        ('gnomad_afr_allele', 'gnomad_afr_allele'),
        ('gnomad_afr', 'gnomad_afr'),
        ('gnomad_amr_allele', 'gnomad_amr_allele'),
        ('gnomad_amr', 'gnomad_amr'),
        ('gnomad_asj_allele', 'gnomad_asj_allele'),
        ('gnomad_asj', 'gnomad_asj'),
        ('gnomad_eas_allele', 'gnomad_eas_allele'),
        ('gnomad_eas', 'gnomad_eas'),
        ('gnomad_fin_allele', 'gnomad_fin_allele'),
        ('gnomad_fin', 'gnomad_fin'),
        ('gnomad_nfe_allele', 'gnomad_nfe_allele'),
        ('gnomad_nfe', 'gnomad_nfe'),
        ('gnomad_oth_allele', 'gnomad_oth_allele'),
        ('gnomad_oth', 'gnomad_oth'),
        ('gnomad_sas_allele', 'gnomad_sas_allele'),
        ('gnomad_sas', 'gnomad_sas'),
        ('hgnc_id', 'hgnc_id'),
        ('hgvsc', 'hgvsc'),
        ('hgvsp', 'hgvsp'),
        ('id', 'id'),
        ('impact', 'impact'),
        ('intron', 'intron'),
        ('motif_feature_consequence_terms',
         'motif_feature_consequence_terms'),
        ('motif_feature_id', 'motif_feature_id'),
        ('motif_name', 'motif_name'),
        ('motif_pos', 'motif_pos'),
        ('motif_score_change', 'motif_score_change'),
        ('maxentscan_alt', 'maxentscan_alt'),
        ('maxentscan_diff', 'maxentscan_diff'),
        ('maxentscan_ref', 'maxentscan_ref'),
        ('most_severe_consequence', 'most_severe_consequence'),
        ('phenotype_or_disease', 'phenotype_or_disease'),
        ('polyphen_prediction', 'polyphen_prediction'),
        ('polyphen_score', 'polyphen_score'),
        ('protein_end', 'protein_end'),
        ('protein_id', 'protein_id'),
        ('protein_start', 'protein_start'),
        ('pubmed', 'pubmed'),
        ('regulatory_feature_consequence_terms',
         'regulatory_feature_consequence_terms'),
        ('seq_region_name', 'seq_region_name'),
        ('sift_prediction', 'sift_prediction'),
        ('sift_score', 'sift_score'),
        ('somatic', 'somatic'),
        ('start', 'start'),
        ('end', 'end'),
        ('strand', 'strand'),
        ('swissprot', 'swissprot'),
        ('transcript_id', 'transcript_id'),
        ('trembl', 'trembl'),
        ('uniparc', 'uniparc'),
        ('variant_allele', 'variant_allele'),
        ('variant_class', 'variant_class'),
    )

    vcf_files = forms.ModelMultipleChoiceField(
        label='Vcf File',
        queryset=None,
        widget=forms.CheckboxSelectMultiple(
        ),
    )
    omim = forms.BooleanField(
        label='omim',
        required=False,
        widget=forms.CheckboxInput(
        ),
    )
    hpo = forms.BooleanField(
        label='hpo',
        required=False,
        widget=forms.CheckboxInput(
        ),
    )
    clinvar = forms.BooleanField(
        label='clinvar',
        required=False,
        widget=forms.CheckboxInput(
        ),
    )
    query_chunks = forms.IntegerField(
        label='Query Chunks',
        initial=199,
    )

    columns = forms.MultipleChoiceField(
        label='Columns',
        choices=choices,
        widget=forms.CheckboxSelectMultiple(
        )
    )

    def __init__(self, user, project_pk, *args, **kwargs):
        super(OVAForm, self).__init__(*args, **kwargs)
        self.fields['vcf_files'].queryset = Vcf.objects.filter(
            user=user).filter(project__pk=project_pk)

        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', 'Submit'))


class ColumnFilterForm(forms.ModelForm):
    class Meta:
        model = ColumnFilter
        exclude = ['entry_date', 'user']
        fields = ['name', 'column', 'option', 'parameters', 'description']

    def __init__(self, *args, **kwargs):
        super(ColumnFilterForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', 'Create Column Filter'))


class FilterSetForm(forms.ModelForm):
    column_filters = forms.ModelMultipleChoiceField(
        queryset=ColumnFilter.objects.all(), required=True)

    class Meta:
        model = FilterSet
        exclude = ['entry_date', 'user']
        fields = ['name', 'column_filters', 'description']

    def __init__(self, *args, **kwargs):
        super(FilterSetForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', 'Create Filter Set'))


# Not used
# class SelectPanelForm(forms.Form):
#     ''' For filtering by position'''
#     panel = forms.ModelMultipleChoiceField(
#         label='Bed File',
#         queryset=None)

#     def __init__(self, user, *args, **kwargs):
#         super(SelectPanelForm, self).__init__(*args, **kwargs)
#         self.fields['panel'].queryset = Panel.objects.filter(
#             Q(curated=True, user__is_superuser=True) | Q(user=user))

#         self.helper = FormHelper()
#         self.helper.add_input(Submit('submit', 'Submit'))
