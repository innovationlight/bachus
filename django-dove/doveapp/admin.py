from django.contrib import admin

from .models import Vcf, Annotation, ColumnFilter, FilterSet
from guardian.admin import GuardedModelAdmin


class VcfAdmin(GuardedModelAdmin):

    list_filter = ['user', 'project', 'panel', 'pipeline', 'run']


class AnnotationAdmin(GuardedModelAdmin):

    list_filter = ['user', 'project', 'panel']


class ColumnFilterAdmin(GuardedModelAdmin):
    pass


class FilterSetAdmin(GuardedModelAdmin):
    pass


admin.site.register(Vcf, VcfAdmin)
admin.site.register(Annotation, AnnotationAdmin)
admin.site.register(ColumnFilter, ColumnFilterAdmin)
admin.site.register(FilterSet, FilterSetAdmin)
