from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404

from django.contrib import messages

from django.contrib.auth.decorators import login_required
from .forms import VcfForm, OVAForm, FilterSetForm, ColumnFilterForm
from .models import Vcf, Annotation, FilterSet
from .tables import VcfTable, AnnotationTable, VariantTable
from doveapp.tasks import annotation_task

from main.models import Project, Panel

from django_tables2.config import RequestConfig
from django_tables2.export.export import TableExport

from pigeonapp.forms import RunForm
from pigeonapp.utils import get_config_content
from pigeonapp.tasks import run_pipeline
from pigeonapp.models import Pipeline

import os
from django.conf import settings
import pandas as pd
# Create Summary
# from .utils.variantclassification import VariantClassification
# import datetime


@login_required
def index(request):
    data = [
        {
            'collection_title': 'Vcf',
            'rows': [
                {
                    'name': 'Upload',
                    'link': 'dove:vcf_upload',
                },
                {
                    'name': 'Browse',
                    'link': 'dove:vcf_browse',
                }
            ]
        },
        {
            'collection_title': 'Annotation',
            'rows': [
                {
                    'name': 'Annotate Vcf with VEP Local',
                    'link': 'dove:ova_vep_local',
                },
                {
                    'name': 'Annotate Vcf with VEP API',
                    'link': 'dove:ova_vep_api',
                },
                {
                    'name': 'Browse',
                    'link': 'dove:annotation_browse',
                }
            ]
        },
    ]

    return render(
        request=request,
        template_name='index.html',
        context={
            "data": data,
            "title": 'Dove',
        }
    )


@login_required
def vcf_upload(request):
    project = Project.objects.filter(
        pk=request.session.get('current_project_pk')
    ).first()
    panel = Panel.objects.filter(
        pk=request.session.get('current_panel_pk')
    ).first()
    if request.method == 'POST':
        form = VcfForm(request.user, request.POST, request.FILES)
        if form.is_valid():
            vcf_form = form.save(commit=False)
            vcf_form.project = form.cleaned_data.get('project')
            vcf_form.panel = form.cleaned_data.get('panel')
            vcf_files = request.FILES.getlist('vcf_file')

            vcf_form.user = request.user

            for vcf_file in vcf_files:
                vcf_form.pk = None
                vcf_form.vcf_file = vcf_file
                vcf_form.save()
                messages.success(
                    request, f"Vcf uploaded: {vcf_form.vcf_file.name}")

            return redirect(
                'dove:index'
            )
        else:
            for msg in form.errors:
                messages.error(request, f"{msg}: {form.errors[msg]}")

            return render(
                request=request,
                template_name='form.html',
                context={
                    'title': 'Upload Vcf',
                    'form': form,
                }
            )

    else:
        form = VcfForm(
            request.user,
            initial={
                'project': project,
                'panel': panel,
            },
        )
    return render(
        request=request,
        template_name='form.html',
        context={
            'title': 'Upload Vcf',
            'form': form,
        }
    )


@login_required
def ova_vep_local(request):
    project_pk = request.session.get('current_project_pk')
    project = Project.objects.filter(
        pk=request.session.get('current_project_pk')
    ).first()
    panel = Panel.objects.filter(
        pk=request.session.get('current_panel_pk')
    ).first()
    pipeline = Pipeline.objects.filter(
        pk=request.session.get('current_pipeline_pk')
    ).first()
    vcfs = Vcf.objects.filter(
        project__pk=request.session.get('current_project_pk')
    ).filter(user=request.user)

    input_files = ' '.join([str(vcf) for vcf in vcfs])
    input_names = ' '.join([str(vcf) for vcf in vcfs])

    if request.method == 'POST':
        form = RunForm(
            request.user,
            project_pk,
            request.POST
        )
        if form.is_valid():
            panel = form.cleaned_data.get('panel')
            pipeline = form.cleaned_data.get('pipeline')

            input_files = form.cleaned_data.get('input_files')

            run, config_content = get_config_content(
                request.user,
                pipeline,
                project,
                panel,
                input_files,
                input_names
            )

            run_pipeline.delay(
                request.user.id,
                run.id,
                config_content,
            )

            messages.success(
                request, f"Annotation run started: {run}")

            return redirect(
                'dove:index'
            )
        else:
            for msg in form.errors:
                messages.error(request, f"{msg}: {form.errors[msg]}")

            return render(
                request=request,
                template_name='form.html',
                context={
                    'title': 'Start Run',
                    'form': form,
                }
            )
    else:
        form = RunForm(
            request.user,
            project_pk,
            initial={
                'project': project,
                'panel': panel,
                'pipeline': pipeline,
                'input_files': input_files,
                'input_names': input_names,
            }
        )
    return render(
        request=request,
        template_name='form.html',
        context={
            'title': 'Start Run',
            'form': form
        }
    )


@login_required
def ova_vep_api(request):

    initial_cols = [
        'CHR', 'POS', 'REF', 'ALT', 'GT', 'AD', 'DP',
        'gene_symbol', 'transcript_id', 'id',
        'minor_allele_freq', 'gnomad', 'canonical',
        'transcript_consequence_terms', 'impact', 'biotype',
        'polyphen_prediction', 'polyphen_score',
        'sift_prediction', 'sift_score',
        'hgvsc', 'hgvsp', 'clin_sig', 'pubmed'
    ]

    if 'current_vcf_id' in request.session:
        current_vcf = Vcf.objects.filter(
            pk=request.session.get('current_vcf_id')
        ).first()
    else:
        current_vcf = None

    project_pk = request.session.get('current_project_pk')

    request.session.get('summary_data')
    if request.method == 'POST':
        form = OVAForm(request.user, project_pk, request.POST)
        if form.is_valid():
            query_chunks = form.cleaned_data.get('query_chunks')
            omim = form.cleaned_data.get('omim')
            hpo = form.cleaned_data.get('hpo')
            clinvar = form.cleaned_data.get('clinvar')
            columns = form.cleaned_data.get('columns')
            vcfs = form.cleaned_data.get('vcf_files')

            for vcf in vcfs:
                annotation_task.delay(
                    request.user.id,
                    vcf.id,
                    query_chunks,
                    columns,
                    omim,
                    hpo,
                    clinvar,
                )

                messages.success(
                    request, f"Annotation started for {vcf.vcf_file.name}")

            return redirect(
                'dove:index'
            )

    else:
        form = OVAForm(
            request.user,
            project_pk,
            initial={
                'vcf_files': [current_vcf],
                'columns': initial_cols
            }
        )
    return render(
        request=request,
        template_name='form.html',
        context={
            'form': form
        }
    )


@login_required
def vcf_browse(request):
    table = VcfTable(
        Vcf.objects.filter(
            project__pk=request.session.get('current_project_pk')
        ).filter(user=request.user)
    )

    RequestConfig(request).configure(table)

    export_format = request.GET.get('_export', None)
    if TableExport.is_valid_format(export_format):
        exporter = TableExport(export_format, table)
        return exporter.response('table.{}'.format(export_format))

    return render(
        request=request,
        template_name='browse.html',
        context={
            'tables': [
                {
                    'title': 'Projects',
                    'table': table
                },
            ]
        }
    )


@login_required
def annotation_browse(request):
    table = AnnotationTable(
        Annotation.objects.filter(
            project__pk=request.session.get('current_project_pk')
        ).filter(user=request.user)
    )

    RequestConfig(request).configure(table)

    export_format = request.GET.get('_export', None)
    if TableExport.is_valid_format(export_format):
        exporter = TableExport(export_format, table)
        return exporter.response('table.{}'.format(export_format))

    return render(
        request=request,
        template_name='browse.html',
        context={
            'tables': [
                {
                    'title': 'Projects',
                    'table': table
                },
            ]
        }
    )


@login_required
def vcf_update(request, pk):
    vcf = get_object_or_404(Vcf, pk=pk)
    form = VcfForm(request.user, request.POST or None, instance=vcf)
    if form.is_valid():
        form.save()
        messages.success(
            request, f"Vcf updated: {vcf}")
        return redirect('dove:vcf_browse')
    return render(
        request=request,
        template_name='form.html',
        context={
            'form': form
        }
    )


@login_required
def annotation_update(request, pk):
    annotation = get_object_or_404(Annotation, pk=pk)
    request.session['current_vcf_id'] = annotation.vcf_id
    return redirect('dove:ova_vep_local')


@login_required
def vcf_delete(request, pk):
    vcf = get_object_or_404(Vcf, pk=pk)
    vcf.delete()
    messages.success(
        request, f"Deleted vcf {vcf.vcf_file.name}")
    return redirect('dove:vcf_browse')


@login_required
def annotation_delete(request, pk):
    annotation = get_object_or_404(Annotation, pk=pk)
    annotation.delete()
    messages.success(
        request, f"Deleted annotation {annotation.annotation_file.name}")
    return redirect('dove:annotation_browse')


@login_required
def variant_filter(request, pk, df=None):
    if pk:
        annotation = get_object_or_404(Annotation, pk=pk)
        annotation_name = annotation.annotation_file.name
        messages.success(
            request, f"Filtering annotation {annotation_name}")

        annotation_file_path = os.path.join(settings.MEDIA_ROOT, annotation_name)
        df = pd.read_csv(annotation_file_path)
    variant_dict = df.to_dict(orient='records')

    table = VariantTable(variant_dict)

    curated_filter_sets = FilterSet.objects.filter(curated=True, user__is_superuser=True)
    custom_filter_sets = FilterSet.objects.filter(user=request.user)

    if request.method == 'POST':

        if request.POST['submit'] == 'Create Column Filter':
            cf_form = ColumnFilterForm(request.POST)
            if cf_form.is_valid():
                cf_form = cf_form.save(commit=False)
                cf_form.user = request.user
                cf_form.save()
                messages.success(
                    request, f"Column filter saved: {cf_form.name}")
            else:
                for msg in cf_form.errors:
                    messages.error(request, f"{msg}: {cf_form.errors[msg]}")
            fs_form = FilterSetForm()

        if request.POST['submit'] == 'Create Filter Set':
            fs_form = FilterSetForm(request.POST)
            if fs_form.is_valid():
                fs_form = fs_form.save(commit=False)
                fs_form.user = request.user
                fs_form.save()
                messages.success(
                    request, f"Column filter saved: {fs_form.name}")
            else:
                for msg in fs_form.errors:
                    messages.error(request, f"{msg}: {fs_form.errors[msg]}")
            cf_form = ColumnFilterForm()
    else:
        cf_form = ColumnFilterForm()
        fs_form = FilterSetForm()
    return render(
        request=request,
        template_name='doveapp/vf.html',
        context={
            'table': table,
            'filter_set_form': FilterSetForm,
            'column_filter_form': ColumnFilterForm,
            'curated_filter_sets': curated_filter_sets,
            'custom_filter_sets': custom_filter_sets,
        }
    )


# @login_required
# def annotation_browse(request):
#     if request.method == 'POST':
#         form = BrowseAnnotationForm(request.user, request.POST)
#         if form.is_valid():
#             action = form.cleaned_data.get('action')[0]
#             annotation_file = form.cleaned_data.get('annotation_file').first()
#             annotation_path = os.path.join(
#                 settings.MEDIA_ROOT, annotation_file.annotation_file.name)
#             if os.path.exists(annotation_path):
#                 annotation_df = pd.read_csv(annotation_path)
#                 annotation_df.drop_duplicates(
#                     list(annotation_df), inplace=True)
#             else:
#                 raise Http404

#             if action == 'filter':
#                 if 'vcf' in request.session:
#                     del request.session['vcf']
#                 request.session['annotation'] = annotation_file.id
#                 return redirect('dove_vf')
#             if action == 'create_summary':
#                 vcf_file = os.path.join(
#                     settings.MEDIA_ROOT, annotation_file.vcf_file.vcf_file.name)

#                 var_cls = VariantClassification()
#                 variants = var_cls.collect_evidences(annotation_df)
#                 for var in variants:
#                     var['significance'] = var_cls.classify_variant(
#                         var['evidences'])

#                 vcf = Vcf(vcf_file)
#                 vcf.get_header_n_meta_info()
#                 summary_data = {
#                     'sample': {
#                         "analysis_date": '{0:%Y-%m-%d %H:%M}'.format(datetime.datetime.now()),
#                         "sample_id": vcf.header[-1].split('_')[-1],
#                         "institution": vcf.header[-1].split('-')[0],
#                         # "panel": vcf.meta_info['genePanel']
#                         "panel": str(annotation_file.panel)
#                     },
#                     "variants": variants
#                 }
#                 summary_data['pathogenic_variants'] = [variant for variant in summary_data['variants'] if variant['significance']['pathogenic'] == 1 or variant['significance']['likely_pathogenic'] == 1]
#                 summary_data['vus_variants'] = [variant for variant in summary_data['variants'] if variant['significance']['uncertain_significance'] == 1]
#                 summary_data['benign_variants'] = [variant for variant in summary_data['variants'] if variant['significance']['benign'] == 1 or variant['significance']['likely_benign'] == 1]
#                 for variant in summary_data['pathogenic_variants']:
#                     variant['evidences'] = ','.join([k for k, v in variant['evidences'].items() if v >= 1])
#                     variant['significance'] = ','.join([k for k, v in variant['significance'].items() if v >= 1])
#                 for variant in summary_data['vus_variants']:
#                     variant['evidences'] = ','.join([k for k, v in variant['evidences'].items() if v >= 1])
#                     variant['significance'] = ','.join([k for k, v in variant['significance'].items() if v >= 1])
#                 for variant in summary_data['benign_variants']:
#                     variant['evidences'] = ','.join([k for k, v in variant['evidences'].items() if v >= 1])
#                     variant['significance'] = ','.join([k for k, v in variant['significance'].items() if v >= 1])

#                 request.session['summary_data'] = summary_data

#                 return redirect('create_summary')

#             if action == 'browse':
#                 return HttpResponse(annotation_df.to_html(index=False))

#             if action == 'download':
#                 annotation_str = StringIO()
#                 annotation_df.to_csv(annotation_str, index=False)
#                 response = HttpResponse(
#                     annotation_str.getvalue(), content_type='text/plain')
#                 response['Content-Disposition'] = 'attachment; filename={}'.format(
#                     str(annotation_file))
#                 return response
#     else:
#         form = BrowseAnnotationForm(request.user)
#     return render(request, 'doveapp/annotation_browse.html', {'form': form})


# @login_required
# def create_summary(request):
#     summary_data = request.session.get('summary_data')


#     if request.method == 'POST':
#         font_config = FontConfiguration()
#         html_string = render_to_string('doveapp/summary_template.html', {
#             'sample': summary_data['sample'],
#             'pathogenic_variants': summary_data['pathogenic_variants'],
#             'vus_variants': summary_data['vus_variants'],
#             'benign_variants': summary_data['benign_variants'],
#             })
#         html = HTML(string=html_string)
#         css = CSS('static/doveapp/css/summary.css', font_config=font_config)
#         summary = html.write_pdf(stylesheets=[css], font_config=font_config)

#         response = HttpResponse(content_type='application/pdf;')
#         response['Content-Disposition'] = 'inline; filename={}.pdf'.format('test')
#         response['Content-Transfer-Encoding'] = 'binary'
#         # with tempfile.NamedTemporaryFile(delete=True) as output:
#         #     output.write(summary)
#         #     output.flush()
#         #     output = open(output.name, 'r')
#         #     response.write(output.read())
#         return response

#     else:

#         return render(request, 'doveapp/summary_preview.html', {
#             'sample': summary_data['sample'],
#             'pathogenic_variants': summary_data['pathogenic_variants'],
#             'vus_variants': summary_data['vus_variants'],
#             'benign_variants': summary_data['benign_variants'],
#         })


# @login_required
# def dove_mga(request):
#     return render(request, 'doveapp/dove_mga.html')


# @login_required
# def dove_vf(request):
#     if 'vcf' in request.session:
#         vcf = VcfModel.objects.get(id=request.session.get('vcf'))
#         with Vcf(os.path.join(settings.MEDIA_ROOT, vcf.vcf_file.name)) as vcf:
#             vcf.vdf = vcf.get_sample_format(concat=True, drop=True)
#             vcf.vdf = vcf.get_variant_info(concat=True, drop=True)
#             df = vcf.vdf
#     if 'annotation' in request.session:
#         annotation = AnnotationModel.objects.get(id=request.session.get('annotation'))
#         df = pd.read_csv(os.path.join(settings.MEDIA_ROOT, annotation.annotation_file.name))

#     columns = ' '.join(str(df.dtypes).replace('\n', ',').split()).split(',')[:-1]

#     VariantFilterFormSet = formset_factory(VariantFilterForm)
#     if request.method == 'POST':
#         formset = VariantFilterFormSet(request.user, request.POST)
#         form = SelectPanelForm(request.user, request.POST)
#         if formset.is_valid():
#             for form in formset:
#                 print(form)
#             pass
#     else:
#         formset = VariantFilterFormSet(initial=[{'column': column} for column in list(df)])
#         form = SelectPanelForm(request.user)
#     return render(request, 'doveapp/dove_vf.html', {'form':form, 'formset':formset})
