from .models import Vcf, Annotation
import os


def create_vcf_model(run, file_path):
    vcf, created = Vcf.objects.get_or_create(
        run=run,
        user=run.user,
        project=run.project,
        panel=run.panel,
        pipeline=run.pipeline,
        vcf_file=file_path,
    )

    if created:
        vcf.save()
    else:
        pass


def create_annotation_model(run, file_path):
    vcf_file_path = os.path.splitext(
        os.path.splitext(
            file_path
        )[0]
    )[0].replace('/annotation/', '/vcf/') + '.vcf'

    vcfgz_exists = Vcf.objects.filter(
        vcf_file=vcf_file_path + '.gz').count()
    vcf_exists = Vcf.objects.filter(vcf_file=vcf_file_path).count()

    if vcf_exists or vcfgz_exists:
        if vcf_exists:
            vcf = Vcf.objects.get(vcf_file=vcf_file_path)
        elif vcfgz_exists:
            vcf = Vcf.objects.get(vcf_file=vcf_file_path + '.gz')

        annotation, created = Annotation.objects.get_or_create(
            annotation_file=file_path,
            user=run.user,
            project=run.project,
            panel=run.panel,
            vcf=vcf
        )
        if created:
            annotation.save()
        else:
            pass
    else:
        pass
