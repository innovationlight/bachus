from django.db import models
from django.contrib.auth.models import User
import datetime
import os

# handle user deletion
from django.contrib.auth import get_user_model

from django.core.validators import FileExtensionValidator
# OverwriteStorage
from django.conf import settings
from django.core.files.storage import FileSystemStorage


class OverwriteStorage(FileSystemStorage):
    '''
    https://gist.github.com/fabiomontefuscolo/1584462
    Muda o comportamento padrão do Django e o faz sobrescrever arquivos de
    mesmo nome que foram carregados pelo usuário ao invés de renomeá-los.
    '''

    def get_available_name(self, name, max_length=None):
        if self.exists(name):
            os.remove(os.path.join(settings.MEDIA_ROOT, name))
        return name


def get_default_user():
    return get_user_model().objects.get_or_create(username='default')[0]


def get_default_institute():
    institute, created = Institute.objects.get_or_create(
        name='default',
        description='default',
    )
    return institute


def get_default_project():
    project, created = Project.objects.get_or_create(
        user=get_default_user(),
        institute=get_default_institute(),
        name='default',
        description='default',
    )
    return project


def get_default_panel():
    panel, created = Panel.objects.get_or_create(
        user=get_default_user(),
        project=get_default_project(),

        name='default',
        description='default',
        curated=False,
        bed_file=os.path.join(
            settings.MEDIA_ROOT,
            'resources',
            'default.bed'
        )[0]
    )
    return panel


class Institute(models.Model):
    iid = models.SlugField(
        max_length=255,
        null=True,
    )
    name = models.TextField()

    def __str__(self):
        return self.name


class Project(models.Model):
    entry_date = models.DateField(default=datetime.datetime.now)
    user = models.ForeignKey(User, on_delete=models.SET(get_default_user))
    institute = models.ForeignKey(
        Institute,
        on_delete=models.SET(get_default_institute),
        null=True
    )
    name = models.SlugField(
        max_length=255,
    )
    description = models.TextField()

    class Meta:
        unique_together = ('user', 'name')

    def save(self, *args, **kwargs):
        self.name = self.name.replace(' ', '_')
        super(Project, self).save(*args, **kwargs)

    def __str__(self):
        return self.name


def bed_file_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'user_data/{0}/{1}/panel/{3}'.format(
        instance.user.id,
        instance.project,
        filename
    )


class Panel(models.Model):
    entry_date = models.DateField(default=datetime.datetime.now)
    user = models.ForeignKey(User, on_delete=models.SET(get_default_user))
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    name = models.SlugField(
        max_length=255,
    )
    description = models.TextField()
    curated = models.BooleanField(null=True)
    bed_file = models.FileField(
        upload_to=bed_file_path,
        storage=OverwriteStorage(),
        validators=[FileExtensionValidator(allowed_extensions=['bed'])]
    )

    class Meta:
        unique_together = ('project', 'name')

    def __str__(self):
        return self.name


class Sample(models.Model):
    entry_date = models.DateField(default=datetime.datetime.now)
    user = models.ForeignKey(User, on_delete=models.SET(get_default_user))
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    name = models.SlugField(
        max_length=255,
    )


class SequenceIndex(models.Model):
    seq_index = models.SlugField(
        max_length=255,
    )
