import os
import pandas as pd
from io import StringIO
from django.conf import settings
import json


class GeneSymbol():

    def get_list(self):
        try:
            df = pd.read_csv(
                os.path.join(
                    settings.MEDIA_ROOT,
                    'resources',
                    'gene_with_protein_product.txt.gz'
                ),
                sep='\t',
                compression='gzip'
            )

            genes = df['Approved Symbol'].tolist()
        except FileNotFoundError:
            genes = []
        return genes

    def gene_json(self):
        gene_list = self.get_list()
        genes = {symbol: None for symbol in gene_list}
        data = json.dumps(genes)
        return data


def bed_create(hg_version, gene_list, exon_start_minus, exon_end_plus):
    if hg_version == 'hg19':
        gff_file = os.path.join(
            settings.MEDIA_ROOT,
            'resources',
            'hg19',
            'gencode.v19.annotation.gff3.csv'
        )

    if hg_version == 'hg38':
        gff_file = os.path.join(
            settings.MEDIA_ROOT,
            'resources',
            'hg38',
            'gencode.v29.annotation.gff3.csv'
        )

    gff = pd.read_csv(gff_file)
    if ',' in gene_list:
        gene_list_df = pd.DataFrame(
            {'gene_symbol': [gene.replace(' ', '') for gene in gene_list.split(',')]})
    else:
        gene_list_df = pd.DataFrame({'gene_symbol': gene_list.split()})
    df = pd.merge(gff, gene_list_df, on='gene_symbol')
    # get rows with gene id/symbol
    df['start'] = df['start'] + exon_start_minus
    df['end'] = df['end'] + exon_end_plus

    df_str = StringIO()
    df.to_csv(df_str, sep='\t', index=False, header=False)
    return df_str.getvalue()


def genes_diseases(input_type, genes_or_diseases):
    cache_path = os.path.join(os.path.expanduser('~'),
                              '.cache', 'dove', 'data')
    df = pd.read_csv(os.path.join(cache_path, 'omim.csv'))
    df = df[df[input_type].str.contains(
        genes_or_diseases.replace(' ', '|'), case=False, na=False) == True]

    return df.to_dict(orient='records')


def genes_panels(genes):
    if ',' in genes:
        genes = pd.DataFrame(
            {'gene_symbol': genes.replace(' ', '').split(',')})
    else:
        genes = pd.DataFrame({'gene_symbol': genes.split(' ')})

    panels = pd.read_csv(os.path.join(
        settings.MEDIA_ROOT, 'resources', 'panels_all.csv'))

    found_panels = pd.DataFrame(columns=['panel', 'found'])
    for i, row in panels.iterrows():
        panel = pd.DataFrame({'gene_symbol': row['gene_symbol'].split(',')})
        comp = compared_df(panel, genes)
        found = comp.loc[lambda x: x['_merge'] == 'both']
        not_found = comp.loc[lambda x: x['_merge'] == 'right_only']
        if len(found) > 0:
            found_panel = pd.DataFrame({
                'panel': row['panel'],
                'found': [','.join(found['gene_symbol'].tolist())],
                'not_found': [','.join(not_found['gene_symbol'].tolist())],
                'gene_count': len(found['gene_symbol'].tolist()),
                'count': '{}/{}'.format(len(found['gene_symbol'].tolist()), len(genes)),
            })
            found_panels = found_panels.append(found_panel, ignore_index=True)
    df = panels.merge(found_panels, on='panel')

    # Styling
    try:
        df.sort_values(by='gene_count', ascending=False, inplace=True)
        df = df[['panel', 'creator', 'sample_type', 'found', 'not_found', 'count']]
    except KeyError:
        pass
    return df.to_dict(orient='records')


def compared_df(df1, df2):
    diff = df1.merge(df2, indicator=True, how='outer',
                     on='gene_symbol')
    return diff
