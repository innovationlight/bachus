from django.urls import path, re_path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.index, name='index'),
    path('index', views.index, name='index'),
    path('chat', views.chat, name='chat'),
    path('flower', views.flower_view, name='flower_view'),
    path('gene_symbol_autocomplete', views.GeneSymbolAutocomplete.as_view(), name='gene_symbol_autocomplete'),
    re_path(r'igv/(?P<track>.+)', views.igv, name='igv'),

    path('project_create', views.project_create, name='project_create'),
    path('panel_create', views.panel_create, name='panel_create'),

    path('institute/browse', views.institute_browse, name='institute_browse'),
    path('project/browse', views.project_browse, name='project_browse'),
    path('panel/browse', views.panel_browse, name='panel_browse'),
    path('panel/browse/curated', views.panel_browse_curated, name='panel_browse_curated'),

    re_path(r'^institute/(\d+)/$', views.institute_current, name='institute_current'),
    re_path(r'^project/(\d+)/$', views.project_current, name='project_current'),
    re_path(r'^panel/(\d+)/$', views.panel_current, name='panel_current'),

    re_path(r'^project/update/(\d+)/$', views.project_update, name='project_update'),
    re_path(r'^panel/update/(\d+)/$', views.panel_update, name='panel_update'),

    re_path(r'^project/delete/(\d+)/$', views.project_delete, name='project_delete'),
    re_path(r'^panel/delete/(\d+)/$', views.panel_delete, name='panel_delete'),

    path('bed_create', views.bed_create, name='bed_create'),

    path('genes_diseases', views.genes_diseases, name='genes_diseases'),
    path('genes_panels', views.genes_panels, name='genes_panels'),

    path('genes_diseases/browse', views.genes_diseases_browse, name='genes_diseases_browse'),
    path('genes_panels/browse', views.genes_panels_browse, name='genes_panels_browse'),
]
