from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.http import HttpResponse

from .models import Institute, Project, Panel
from .forms import ProjectForm, PanelForm
from .forms import ChatForm
from .tables import InstituteTable, ProjectTable, PanelTable, PanelCuratedTable
from .tables import GeneDiseaseTable, PanelGeneTable

from django_tables2.config import RequestConfig
from django_tables2.export.export import TableExport

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test
# from django.contrib.auth.mixins import LoginRequiredMixin
# from django.contrib.auth.mixins import UserPassesTestMixin
# from django.views.generic.edit import FormView

# bed, disease-gene, gene-panel
from dal import autocomplete
from .forms import BedCreateForm, GeneDiseaseForm, GenePanelForm
from . import utils


def index(request):
    data = [
        {
            'collection_title': 'Project',
            'rows': [
                {
                    'name': 'New',
                    'link': 'main:project_create',
                },
                {
                    'name': 'Browse',
                    'link': 'main:project_browse',
                },
            ]
        },
        {
            'collection_title': 'Panel',
            'rows': [
                {
                    'name': 'New',
                    'link': 'main:panel_create',
                },
                {
                    'name': 'Browse',
                    'link': 'main:panel_browse',
                },
                {
                    'name': 'Browse Curated',
                    'link': 'main:panel_browse_curated',
                },
            ]
        },
        {
            'collection_title': 'Utility',
            'rows': [
                {
                    'name': 'New Bed file',
                    'link': 'main:bed_create',
                },
                {
                    'name': 'Search genes&diseases',
                    'link': 'main:genes_diseases',
                },
                {
                    'name': 'Search genes in panels',
                    'link': 'main:genes_panels',
                },
            ]
        },
    ]

    return render(
        request=request,
        template_name='index.html',
        context={
            "data": data,
            "title": 'Welcome',
        }
    )


@login_required
def chat(request):
    if request.method == 'POST':
        form = ChatForm(request.user, request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')

            messages.success(
                request, f"Messaging to {username}")

            return redirect(
                'dialogs_detail',
                username=username,
            )
        else:
            for msg in form.errors:
                messages.error(request, f"{msg}: {form.errors[msg]}")

            return render(
                request=request,
                template_name='form.html',
                context={
                    'title': 'Chat',
                    'form': form,
                }
            )

    else:
        form = ChatForm(request.user)
    return render(
        request=request,
        template_name='form.html',
        context={
            'title': 'Chat',
            'form': form,
        }
    )


def igv(request, track):
    import json
    import os

    if track.endswith('bam'):
        track_data = {
            "type": "alignment",
            "format": "bam",
            "name": track.split('/')[-1].split('_')[0],
            "url": track,
            "indexURL": os.path.splitext(track)[0] + '.bai'
        }
    if track.endswith('vcf.gz'):
        track_data = {
            "type": "variant",
            "format": "vcf",
            "name": track.split('/')[-1].split('_')[0],
            "url": track,
            "indexURL": track + '.tbi',
            "squishedCallHeight": 1,
            "expandedCallHeight": 4,
            "displayMode": "squished",
            "visibilityWindow": 1000
        }
    track_data = json.dumps(track_data)

    return render(
        request=request,
        template_name='igv/igv.html',
        context={
            'track_data': track_data
        }
    )


@user_passes_test(lambda u: u.is_staff and u.is_superuser)
def flower_view(request):
    '''passes the request back up to nginx for internal routing'''
    response = HttpResponse()
    path = request.get_full_path()
    path = path.replace('flower', 'flower-internal', 1)
    response['X-Accel-Redirect'] = path
    return response


@login_required
def project_create(request):
    institute = Institute.objects.filter(
        pk=request.session.get('current_institute_pk')
    ).first()
    if request.method == 'POST':
        form = ProjectForm(request.POST)
        if form.is_valid():
            project_form = form.save(commit=False)
            project_form.user = request.user
            project_form.save()

            messages.success(
                request, f"New project created: {project_form.name}")

            return redirect(
                'main:index'
            )
        else:
            for msg in form.errors:
                messages.error(request, f"{msg}: {form.errors[msg]}")

            return render(
                request=request,
                template_name='form.html',
                context={
                    'title': 'New Project',
                    'form': form,
                }
            )

    else:
        form = ProjectForm(
            initial={
                'institute': institute
            },
        )
    return render(
        request=request,
        template_name='form.html',
        context={
            'title': 'New Project',
            'form': form,
        }
    )


@login_required
def panel_create(request):
    project = Project.objects.filter(
        pk=request.session.get('current_project_pk')
    ).first()

    if request.method == 'POST':
        form = PanelForm(request.user, request.POST, request.FILES)
        if form.is_valid():
            panel_form = form.save(commit=False)
            panel_form.user = request.user
            panel_form.save()

            messages.success(
                request, f"New panel created: {panel_form.name}")

            return redirect(
                'main:index'
            )
        else:
            for msg in form.errors:
                messages.error(request, f"{msg}: {form.errors[msg]}")

            return render(
                request=request,
                template_name='form.html',
                context={
                    'title': 'New Panel',
                    'form': form,
                }
            )

    else:
        form = PanelForm(
            request.user,
            initial={
                'project': project
            },
        )
    return render(
        request=request,
        template_name='form.html',
        context={
            'title': 'New Panel',
            'note': '<a href="https://genome.ucsc.edu/FAQ/FAQformat.html">Bed file specifications</a>',
            'form': form,
        }
    )


@login_required
def institute_browse(request):
    table = InstituteTable(Institute.objects.all())

    RequestConfig(request).configure(table)

    export_format = request.GET.get('_export', None)
    if TableExport.is_valid_format(export_format):
        exporter = TableExport(export_format, table)
        return exporter.response('table.{}'.format(export_format))

    return render(
        request=request,
        template_name='browse.html',
        context={
            'tables': [
                {
                    'title': 'Institutes',
                    'table': table
                },
            ]
        }
    )


@login_required
def project_browse(request):
    table = ProjectTable(Project.objects.filter(user=request.user))

    RequestConfig(request).configure(table)

    export_format = request.GET.get('_export', None)
    if TableExport.is_valid_format(export_format):
        exporter = TableExport(export_format, table)
        return exporter.response('table.{}'.format(export_format))

    return render(
        request=request,
        template_name='browse.html',
        context={
            'tables': [
                {
                    'title': 'Projects',
                    'table': table
                },
            ]
        }
    )


@login_required
def panel_browse(request):
    table = PanelTable(
        Panel.objects.filter(
            project__pk=request.session.get('current_project_pk')
        ).filter(user=request.user)
    )

    RequestConfig(request).configure(table)

    export_format = request.GET.get('_export', None)
    if TableExport.is_valid_format(export_format):
        exporter = TableExport(export_format, table)
        return exporter.response('table.{}'.format(export_format))

    return render(
        request=request,
        template_name='browse.html',
        context={
            'tables': [
                {
                    'title': 'Panels',
                    'table': table
                },
            ]
        }
    )


@login_required
def panel_browse_curated(request):
    table = PanelCuratedTable(
        Panel.objects.filter(curated=True, user__is_superuser=True)
    )

    RequestConfig(request).configure(table)

    export_format = request.GET.get('_export', None)
    if TableExport.is_valid_format(export_format):
        exporter = TableExport(export_format, table)
        return exporter.response('table.{}'.format(export_format))

    return render(
        request=request,
        template_name='browse.html',
        context={
            'tables': [
                {
                    'title': 'Curated Panels',
                    'table': table
                },
            ]
        }
    )


@login_required
def project_update(request, pk):
    project = get_object_or_404(Project, pk=pk)
    form = ProjectForm(request.POST or None, instance=project)
    if form.is_valid():
        form.save()
        messages.success(
            request, f"Project updated: {project.name}")
        return redirect('main:project_browse')
    return render(
        request=request,
        template_name='form.html',
        context={
            'form': form
        }
    )


@login_required
def panel_update(request, pk):
    panel = get_object_or_404(Panel, pk=pk)
    form = PanelForm(request.user, request.POST or None, instance=panel)
    if form.is_valid():
        form.save()
        messages.success(
            request, f"Panel updated: {panel.name}")
        return redirect('main:panel_browse')
    return render(
        request=request,
        template_name='form.html',
        context={
            'form': form
        }
    )


@login_required
def project_delete(request, pk):
    project = get_object_or_404(Project, pk=pk)
    project.delete()
    messages.success(
        request, f"Deleted project {project.name}")
    return redirect('main:project_browse')


@login_required
def panel_delete(request, pk):
    panel = get_object_or_404(Panel, pk=pk)
    panel.delete()
    messages.success(
        request, f"Deleted panel {panel.name}")
    return redirect('main:panel_browse')


@login_required
def institute_current(request, pk):
    institute = get_object_or_404(Institute, pk=pk)
    request.session['current_institute_pk'] = pk
    request.session['current_institute_name'] = institute.name
    messages.success(
        request, f"Current  institute set to {institute.name}")
    return redirect('main:index')


@login_required
def project_current(request, pk):
    project = get_object_or_404(Project, pk=pk)
    request.session['current_project_pk'] = pk
    request.session['current_project_name'] = project.name
    messages.success(
        request, f"Current  project set to {project.name}")
    return redirect('main:index')


@login_required
def panel_current(request, pk):
    panel = get_object_or_404(Panel, pk=pk)
    request.session['current_panel_pk'] = pk
    request.session['current_panel_name'] = panel.name
    messages.success(
        request, f"Current  panel set to {panel.name}")
    return redirect('main:index')


class GeneSymbolAutocomplete(autocomplete.Select2ListView):
    def get_list(self):
        gene_symbol = utils.GeneSymbol()
        return gene_symbol.get_list()


@login_required
def bed_create(request):
    if request.method == 'POST':
        form = BedCreateForm(request.POST)
        if form.is_valid():
            hg_version = form.cleaned_data.get('hg_version')
            gene_list = form.cleaned_data.get('gene_list')
            exon_start_minus = form.cleaned_data.get('exon_start_minus')
            exon_end_plus = form.cleaned_data.get('exon_end_plus')

            data = utils.bed_create(
                hg_version,
                gene_list,
                exon_start_minus,
                exon_end_plus
            )

            response = HttpResponse(
                data, content_type='text/plain')
            response['Content-Disposition'] = 'attachment; filename=genes.bed'
            return response

    else:
        form = BedCreateForm()
    return render(
        request=request,
        template_name='form.html',
        context={
            'title': 'Create Bed file',
            'note': 'Bed includes all exons from all transcripts.',
            'form': form,
            # 'js_data': utils.get_gene_symbols(),
        }
    )


def genes_diseases(request):
    if request.method == 'POST':
        form = GeneDiseaseForm(request.POST)
        if form.is_valid():
            input_type = form.cleaned_data.get('input_type')
            genes_or_diseases = form.cleaned_data.get('genes_or_diseases')

            request.session['data'] = utils.genes_diseases(
                input_type, genes_or_diseases)
            return redirect(
                'main:genes_diseases_browse'
            )

    else:
        form = GeneDiseaseForm()
    return render(
        request=request,
        template_name='form.html',
        context={
            'title': 'Search Genes and Diseases',
            'form': form,
            # 'js_data': utils.get_gene_symbols(),
        }
    )


def genes_panels(request):
    if request.method == 'POST':
        form = GenePanelForm(request.POST)
        if form.is_valid():
            genes = form.cleaned_data.get('genes')
            print(genes)

            request.session['data'] = utils.genes_panels(genes)
            return redirect(
                'main:genes_panels_browse'
            )

    else:
        form = GenePanelForm(
            initial={
            }
        )
    return render(
        request=request,
        template_name='form.html',
        context={
            'title': 'Search Genes in Panels',
            'form': form,
        }
    )


def genes_diseases_browse(request):
    table = GeneDiseaseTable(
        request.session.get('data')
    )
    RequestConfig(request).configure(table)

    export_format = request.GET.get('_export', None)
    if TableExport.is_valid_format(export_format):
        exporter = TableExport(export_format, table)
        return exporter.response('table.{}'.format(export_format))

    return render(
        request=request,
        template_name='browse.html',
        context={
            'tables': [
                {
                    'name': 'Genes Diseases',
                    'table': table
                },
            ]
        }
    )


def genes_panels_browse(request):
    table = PanelGeneTable(
        request.session.get('data')
    )
    RequestConfig(request).configure(table)

    export_format = request.GET.get('_export', None)
    if TableExport.is_valid_format(export_format):
        exporter = TableExport(export_format, table)
        return exporter.response('table.{}'.format(export_format))

    return render(
        request=request,
        template_name='browse.html',
        context={
            'tables': [
                {
                    'title': 'Panel Search',
                    'table': table,
                    'note': 'Özellikle tümör kitleri genin belli ekzonları hedefleyebiliyor. Çalışmanıza uygun mu diye kontrol ediniz.'
                },
            ]
        }
    )
