from django import forms
from django.forms import ModelForm
from .models import Project, Panel
from django.contrib.auth.models import User

from dove.utils.bed import Bed
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

from registration.forms import RegistrationFormUniqueEmail
from captcha.fields import CaptchaField

from dal import autocomplete
from . import utils


class ChatForm(forms.Form):
    username = forms.SlugField()

    def __init__(self, user, *args, **kwargs):
        super(ChatForm, self).__init__(*args, **kwargs)
        self.user = user

        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', 'Submit'))

    def clean(self):
        cleaned_data = super().clean()
        username = cleaned_data.get("username")

        if username == self.user.username:
            raise forms.ValidationError(
                "Well, of course I know him. He's me."
            )
        if not User.objects.filter(username=username).exists():
            raise forms.ValidationError(
                'User not found'
            )


class CustomRegistration(RegistrationFormUniqueEmail):
    captcha = CaptchaField()


class ProjectForm(ModelForm):

    class Meta:
        model = Project
        fields = ['name', 'institute', 'description']
        exclude = ['user']
        widgets = {
            'institute': forms.Select(
            ),
        }

    def __init__(self, *args, **kwargs):
        super(ProjectForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', 'Submit'))


class PanelForm(ModelForm):

    class Meta:
        model = Panel
        fields = ['project', 'name', 'description', 'bed_file', 'curated']
        exclude = ['user']
        widgets = {
            'project': forms.Select(
            ),
        }

    def __init__(self, user, *args, **kwargs):
        super(PanelForm, self).__init__(*args, **kwargs)
        self.fields['project'].queryset = Project.objects.filter(
            user=user)
        if not user.is_staff:
            del self.fields['curated']

        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', 'Submit'))

    def clean_bed_file(self):
        bed_file = self.cleaned_data["bed_file"]
        if not bed_file.name.endswith('.bed'):
            raise forms.ValidationError(
                'Please upload a bed file'
            )
        try:
            bed = Bed(
                bed_file.file.read().decode('utf-8'),
                'str'
            )
            bed.from_string()
        except:
            raise forms.ValidationError(
                'Something is wrong with your file'
            )
        return bed_file


class BedCreateForm(forms.Form):
    defined_actions = (('browse', 'Browse'),
                       ('download', 'Download'))
    choices = (
        ('hg19', 'hg19'),
        ('hg38', 'hg38'))
    gene_list = forms.CharField(
        label='Gene Symbol',
        min_length=1,
        strip=True,
        widget=forms.TextInput(
            attrs={
            }
        )
    )
    exon_start_minus = forms.IntegerField(
        label='Exon Start Minus', initial=-30)
    exon_end_plus = forms.IntegerField(label='Exon End Plus', initial=30)

    hg_version = forms.ChoiceField(
        label='Human Genome Version',
        choices=choices,
        widget=forms.Select(
        ),
    )

    def __init__(self, *args, **kwargs):
        super(BedCreateForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', 'Submit'))


class GeneDiseaseForm(forms.Form):
    defined_actions = (('browse', 'Browse'),
                       ('download', 'Download'))
    choices = (
        ('gene_symbol', 'gene_symbol'),
        ('omim_phenotypes', 'omim_phenotypes'),
    )
    genes_or_diseases = forms.CharField(
        label='Genes or Diseases',
        required=False,
        min_length=1,
        strip=True,
        widget=forms.TextInput(
            attrs={
            }
        )
    )
    input_type = forms.ChoiceField(
        label='input type',
        choices=choices,
        widget=forms.Select(
        ),
    )

    def __init__(self, *args, **kwargs):
        super(GeneDiseaseForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', 'Submit'))


class GenePanelForm(forms.Form):
    gene_symbol = utils.GeneSymbol()
    genes = autocomplete.Select2ListCreateChoiceField(
        choice_list=gene_symbol.get_list(),
        widget=autocomplete.TagSelect2(url='main:gene_symbol_autocomplete')
    )

    def __init__(self, *args, **kwargs):
        super(GenePanelForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', 'Submit'))
