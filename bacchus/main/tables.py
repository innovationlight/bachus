import django_tables2 as tables
from .models import Institute, Project, Panel


class InstituteTable(tables.Table):
    current = tables.LinkColumn('main:institute_current', args=[
        tables.A('pk')], orderable=False, empty_values=())

    class Meta:
        model = Institute
        exclude = ('id',)
        fields = ('current', 'iid', 'name')

    def render_current(self):
        return '✔'


class ProjectTable(tables.Table):
    update = tables.LinkColumn('main:project_update', args=[
        tables.A('pk')], orderable=False, empty_values=())
    delete = tables.LinkColumn('main:project_delete', args=[
        tables.A('pk')], orderable=False, empty_values=())
    current = tables.LinkColumn('main:project_current', args=[
        tables.A('pk')], orderable=False, empty_values=())

    class Meta:
        model = Project
        exclude = ('id', 'user')
        fields = ('current', 'entry_date', 'name', 'description', 'update')

    def render_update(self):
        return '✎'

    def render_delete(self):
        return '✘'

    def render_current(self):
        return '✔'


class PanelTable(tables.Table):
    update = tables.LinkColumn('main:panel_update', args=[
        tables.A('pk')], orderable=False, empty_values=())
    delete = tables.LinkColumn('main:panel_delete', args=[
        tables.A('pk')], orderable=False, empty_values=())
    current = tables.LinkColumn('main:panel_current', args=[
        tables.A('pk')], orderable=False, empty_values=())

    class Meta:
        model = Panel
        exclude = ('id', 'user')
        fields = ('current', 'entry_date', 'project', 'name',
                  'description', 'bed_file', 'update', 'delete')

    def render_update(self):
        return '✎'

    def render_delete(self):
        return '✘'

    def render_current(self):
        return '✔'


class PanelCuratedTable(tables.Table):
    current = tables.LinkColumn('main:panel_current', args=[
        tables.A('pk')], orderable=False, empty_values=())

    class Meta:
        model = Panel
        exclude = ('id',)
        fields = ('current', 'entry_date', 'name', 'user',
                  'description', 'bed_file')

    def render_current(self):
        return '✔'


class GeneDiseaseTable(tables.Table):
    gene_symbol = tables.Column()
    omim_phenotypes = tables.Column()


class PanelGeneTable(tables.Table):
    panel = tables.Column()
    sample_type = tables.Column()
    creator = tables.Column()
    found = tables.Column()
    not_found = tables.Column()
    count = tables.Column()
