from django.contrib import admin
from guardian.admin import GuardedModelAdmin

from .models import Institute, Project, Panel


class InstituteAdmin(GuardedModelAdmin):
    pass


class ProjectAdmin(GuardedModelAdmin):
    pass


class PanelAdmin(GuardedModelAdmin):
    pass


admin.site.register(Institute, InstituteAdmin)
admin.site.register(Project, ProjectAdmin)
admin.site.register(Panel, PanelAdmin)

# Register your models here.
