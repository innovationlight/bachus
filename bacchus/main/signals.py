from django.contrib.auth.models import Group
from registration.signals import user_activated
from django.dispatch import receiver


@receiver(user_activated)
def my_callback(sender, user, request, **kwargs):
    if user.email.endswith('gen-era.com.tr'):
        group, created = Group.objects.get_or_create(name='Gen-Era')
        group.user_set.add(user)
    else:
        group, created = Group.objects.get_or_create(name='Default')
        group.user_set.add(user)
