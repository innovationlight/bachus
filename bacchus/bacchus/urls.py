"""bacchus URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from filebrowser.sites import site

from django.contrib import admin
from django.urls import path, include

from django.conf import settings
from django.conf.urls.static import static

# rest_framework
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets

# Machina Forum
from machina import urls as machina_urls

# Serializers define the API representation.


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'is_staff')


class UserViewSet(viewsets.ModelViewSet):
    '''ViewSets define the view behavior.'''
    queryset = User.objects.all()
    serializer_class = UserSerializer


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'users', UserViewSet)

urlpatterns = [
    # filebrowser
    path('admin/filebrowser/', site.urls),
    path('grappelli/', include('grappelli.urls')),

    path('admin/', admin.site.urls),

    # machina
    path('forum/', include(machina_urls)),

    # django-registration-redux
    path('accounts/', include('registration.backends.admin_approval.urls')),
    path('accounts/', include('allauth.urls')),
    path('accounts/', include('django.contrib.auth.urls')),

    # bacchus apps
    path('', include('main.urls')),
    path('pigeon/', include('pigeonapp.urls')),
    path('dove/', include('doveapp.urls')),
    path('picus/', include('picusapp.urls')),

    # todo
    path('todo/', include('todo.urls', namespace="todo")),

    # path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls')),

    # django-private-chat
    path('chat/', include('django_private_chat.urls')),

    # wiki
    path('notifications/', include('django_nyt.urls')),
    path('wiki/', include('wiki.urls')),

    # captcha
    path('captcha/', include('captcha.urls')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
