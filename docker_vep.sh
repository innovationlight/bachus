TAG="latest"

# Download docker install script
curl -fsSL https://get.docker.com -o get-docker.sh
# run it
sudo sh get-docker.sh
# add user to docker group
sudo usermod -aG docker "$USER"

# Install vep docker
docker pull ensemblorg/ensembl-vep:"$TAG"

# Download vep data and all plugins
mkdir "$HOME"/vep_data
chmod -R a+rwx "$HOME"/vep_data
docker run -t -i -v "$HOME"/vep_data:/opt/vep/.vep ensemblorg/ensembl-vep:"$TAG" perl INSTALL.pl -a cfp -s homo_sapiens_refseq -y GRCh37 -g all
